/**
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include "esp_err.h"

/*
 * esp_netif_init() and esp_event_loop_create_default() MUST be called prior to
 * calling these functions.
 */
esp_err_t wifi_init_sta(unsigned char ssid[], unsigned char wifi_pass[],
			int timeout_sec);
void wifi_init_softap(void);

esp_err_t wifi_stop_events(void);

void ap_mode(void);
