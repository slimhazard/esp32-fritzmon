/**
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <string.h>
#include <inttypes.h>

#include <esp_wifi.h>
#include <esp_event.h>
#include <esp_log.h>
#include <esp_sleep.h>
#include <nvs_flash.h>
#include <soc/gpio_reg.h>
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_timer.h"
#include "expat.h"
#include "driver/gpio.h"

#include "wifi.h"
#include "storage.h"

#define WIFI_GOOD (CONFIG_GPIO_WIFI_GOOD)
#define WIFI_DOWN (CONFIG_GPIO_WIFI_DOWN)
#define DSL_GOOD  (CONFIG_GPIO_DSL_GOOD)
#define DSL_DOWN  (CONFIG_GPIO_DSL_DOWN)
#define IP_GOOD   (CONFIG_GPIO_IP_GOOD)
#define IP_DOWN   (CONFIG_GPIO_IP_DOWN)
#define RUN       (CONFIG_GPIO_RUN)
#define CFG_REQ   (CONFIG_GPIO_CONFIG)
#define VERBOSE   (CONFIG_GPIO_VERBOSE)

#define PIN_MASK ((1U << WIFI_GOOD) | (1U << WIFI_DOWN) | (1U << DSL_GOOD) | \
		  (1U << DSL_DOWN) | (1U << IP_GOOD) | (1U << IP_DOWN))

#define XML_BUFLEN (32)

#define DSL_STATUS_TAG "NewPhysicalLinkStatus"
#define IP_STATUS_TAG "NewConnectionStatus"
#define DSL_GOOD_VAL "Up"
#define IP_GOOD_VAL "Connected"

#define DSL_PATH "/igdupnp/control/WANCommonIFC1"
#define IP_PATH  "/igdupnp/control/WANIPConn1"
#define DSL_SOAP_ACTION "urn:schemas-upnp-org:service:WANCommonInterfaceConfig:1#GetCommonLinkProperties"
#define IP_SOAP_ACTION "urn:schemas-upnp-org:service:WANIPConnection:1#GetStatusInfo"

/* esp_app_desc_t project_name[32] + "/" + version[32] + "\0" */
#define UA_LEN  (66)
#define URL_LEN (1024)

#define LED_ON_MS (50)

struct xml_state {
	char		buf[XML_BUFLEN];
	const char	*tag_name;
	int		len;
	bool		in_status;
};

struct req_state {
	const char	*name;
	XML_Parser	parser;
};

RTC_FAST_ATTR struct config cfg;
RTC_FAST_ATTR char ua[UA_LEN];

extern const char   dsl_body[]   asm("_binary_dsl_req_body_xml_start");
extern const char   ip_body[]    asm("_binary_ip_req_body_xml_start");
extern const size_t dsl_body_len asm("dsl_req_body_xml_length");
extern const size_t ip_body_len  asm("ip_req_body_xml_length");

static const char *TAG = "app";

static gpio_num_t wifi_status_pin = WIFI_DOWN, dsl_status_pin = DSL_DOWN,
	ip_status_pin = IP_DOWN;

/* Not allocating this on the stack */
static char url[URL_LEN];

static void
xml_start_hndlr(void *userData, const XML_Char *name, const XML_Char **attrs)
{
	struct xml_state *state = (struct xml_state *)userData;
	(void)attrs;

	if (strcmp(name, state->tag_name) == 0) {
		state->in_status = true;
		return;
	}
	state->in_status = false;
}

static inline void
xml_end_hndlr(void *userData, const char *good_val, gpio_num_t *status_pin,
	      gpio_num_t status_good)
{
	struct xml_state *state = (struct xml_state *)userData;

	if (state->in_status &&
	    strncmp(state->buf, good_val, state->len) == 0)
		*status_pin = status_good;
	state->in_status = false;
}

static void
dsl_end_hndlr(void *userData, const XML_Char *name)
{
	(void)name;

	xml_end_hndlr(userData, DSL_GOOD_VAL, &dsl_status_pin, DSL_GOOD);
}

static void
ip_end_hndlr(void *userData, const XML_Char *name)
{
	(void)name;

	xml_end_hndlr(userData, IP_GOOD_VAL, &ip_status_pin, IP_GOOD);
}

static void
xml_data_hndlr(void *userData, const XML_Char *data, int len)
{
	struct xml_state *state = (struct xml_state *)userData;

	if (!state->in_status)
		return;
	if (state->len + len > XML_BUFLEN) {
		ESP_LOGE(TAG, "XML parser: buffer overflow");
		return;
	}
	memcpy(state->buf + state->len, data, len);
	state->len += len;
}

static esp_err_t
req_evt_hndlr(esp_http_client_event_t *evt)
{
	struct req_state *state = (struct req_state *)evt->user_data;
	HttpStatus_Code status;
	enum XML_Error xml_err;

	switch (evt->event_id) {
	case HTTP_EVENT_ERROR:
		ESP_LOGE(TAG, "%s request: HTTP error event", state->name);
		return (ESP_FAIL);
	case HTTP_EVENT_ON_DATA:
		if ((status = esp_http_client_get_status_code(evt->client))
		    != HttpStatus_Ok) {
			ESP_LOGW(TAG, "%s response status: %d", state->name,
				 status);
			return (ESP_FAIL);
		}
		if (esp_http_client_is_chunked_response(evt->client)) {
			ESP_LOGE(TAG, "%s request: "
				 "chunked encoding not supported", state->name);
			return (ESP_ERR_NOT_SUPPORTED);
		}
		if (XML_Parse(state->parser, evt->data, evt->data_len, false)
		    != XML_STATUS_OK) {
			xml_err = XML_GetErrorCode(state->parser);
			ESP_LOGE(TAG, "%s client: XML parse error: %d (%s)",
				 state->name, xml_err, XML_ErrorString(xml_err));
			return (ESP_ERR_INVALID_ARG);
		}
		break;
	case HTTP_EVENT_ON_FINISH:
		if (XML_Parse(state->parser, NULL, 0, true) != XML_STATUS_OK) {
			xml_err = XML_GetErrorCode(state->parser);
			ESP_LOGE(TAG, "%s client: XML parse error: %d (%s)",
				 state->name, xml_err, XML_ErrorString(xml_err));
			return (ESP_ERR_INVALID_ARG);
		}
		break;
	case HTTP_EVENT_ON_CONNECTED:
		ESP_LOGD(TAG, "HTTP event: ON_CONNECTED");
		break;
	case HTTP_EVENT_HEADERS_SENT:
		ESP_LOGD(TAG, "HTTP event: HEADERS_SENT");
		break;
	case HTTP_EVENT_ON_HEADER:
		ESP_LOGD(TAG, "HTTP event: ON_HEADER");
		break;
	case HTTP_EVENT_DISCONNECTED:
		ESP_LOGD(TAG, "HTTP event: DISCONNECTED");
		break;
	default:
		ESP_LOGW(TAG, "Invalid HTTP event: %d", evt->event_id);
		break;
	}
	return (ESP_OK);
}

static void
status_and_sleep(void)
{
	ESP_ERROR_CHECK(gpio_set_level(wifi_status_pin, 1));
	ESP_ERROR_CHECK(gpio_set_level(dsl_status_pin, 1));
	ESP_ERROR_CHECK(gpio_set_level(ip_status_pin, 1));
	vTaskDelay(LED_ON_MS / portTICK_PERIOD_MS);
	ESP_ERROR_CHECK(gpio_set_level(wifi_status_pin, 0));
	ESP_ERROR_CHECK(gpio_set_level(dsl_status_pin, 0));
	ESP_ERROR_CHECK(gpio_set_level(ip_status_pin, 0));
	ESP_ERROR_CHECK(gpio_set_level(RUN, 0));

	ESP_ERROR_CHECK(wifi_stop_events());
	ESP_ERROR_CHECK(esp_wifi_disconnect());
	ESP_ERROR_CHECK(esp_wifi_stop());
	ESP_ERROR_CHECK(esp_wifi_deinit());

	ESP_ERROR_CHECK(esp_sleep_pd_config(ESP_PD_DOMAIN_RC_FAST,
					    ESP_PD_OPTION_ON));

	ESP_LOGI(TAG, "Sleep for %" PRIu64 "s after %" PRId64 "µs since boot",
		 cfg.sleep_intvl, esp_timer_get_time());
	esp_deep_sleep(cfg.sleep_intvl * 1000 * 1000);
}

static void
run_led_off(void *args)
{
	ESP_ERROR_CHECK(gpio_set_level(RUN, 0));
}

void
app_main(void)
{
	nvs_handle_t nvs_hndl;
	esp_err_t ret;
	gpio_config_t output_conf = {
		.intr_type = GPIO_INTR_DISABLE,
		.pin_bit_mask = PIN_MASK | (1U << RUN),
		.mode = GPIO_MODE_OUTPUT,
		.pull_up_en = GPIO_PULLUP_DISABLE,
		.pull_down_en = GPIO_PULLDOWN_ENABLE,
	};
	gpio_config_t input_conf = {
		.intr_type = GPIO_INTR_DISABLE,
		.pin_bit_mask = (1U << CFG_REQ) | (1U << VERBOSE),
		.mode = GPIO_MODE_INPUT,
		.pull_up_en = GPIO_PULLUP_DISABLE,
		.pull_down_en = GPIO_PULLDOWN_ENABLE,
	};
	const esp_timer_create_args_t timer_conf = {
		.callback = &run_led_off,
		.name = "run_led_off",
	};
	struct req_state req_state = {
		.name = "DSL",
	};
	struct xml_state xml_state = {
		.tag_name = DSL_STATUS_TAG,
		.len = 0,
		.in_status = false,
	};
	esp_http_client_config_t client_cfg = {
		.method = HTTP_METHOD_POST,
		.path = DSL_PATH,
		.user_data = &req_state,
		.user_agent = ua,
		.event_handler = req_evt_hndlr,
	};
	esp_http_client_handle_t client;
	esp_timer_handle_t timer_hndl;

	ESP_ERROR_CHECK(nvs_flash_init());
	ESP_ERROR_CHECK(esp_netif_init());
	ESP_ERROR_CHECK(esp_event_loop_create_default());

	ESP_ERROR_CHECK(gpio_config(&output_conf));
	REG_WRITE(GPIO_OUT_W1TC_REG, PIN_MASK);
	ESP_ERROR_CHECK(gpio_set_level(RUN, 1));
	ESP_ERROR_CHECK(esp_timer_create(&timer_conf, &timer_hndl));

	ESP_ERROR_CHECK(gpio_config(&input_conf));
	if (gpio_get_level(VERBOSE) == 1)
		esp_log_level_set("*", CONFIG_LOG_MAXIMUM_LEVEL);

	if (gpio_get_level(CFG_REQ) == 1) {
		ESP_LOGI(TAG, "Re-configuration requested, starting AP mode");
		ap_mode();
		return;
	}

	if (esp_reset_reason() != ESP_RST_DEEPSLEEP) {
		const esp_app_desc_t *app_desc;

		ESP_LOGI(TAG, "Read config from NVS");
		ESP_ERROR_CHECK(nvs_open("settingsNS", NVS_READONLY, &nvs_hndl));
		ret = get_config(nvs_hndl, &cfg);
		nvs_close(nvs_hndl);
		if (ret != ESP_OK || cfg.ssid[0] == '\0') {
			if (cfg.ssid[0] == '\0')
				ESP_LOGW(TAG, "SSID in NVS is empty");
			ESP_LOGI(TAG, "Starting AP mode");
			ap_mode();
			return;
		}

		app_desc = esp_app_get_description();
		assert(app_desc != NULL);
		snprintf(ua, UA_LEN, "%s/%s", app_desc->project_name,
			 app_desc->version);
	}

	ESP_ERROR_CHECK(esp_timer_start_once(timer_hndl, LED_ON_MS * 1000));

	if (wifi_init_sta((unsigned char *)cfg.ssid, (unsigned char *)cfg.pass,
			  cfg.wifi_tmo) != ESP_OK) {
		ESP_LOGE(TAG, "Wifi station connect failed");
		status_and_sleep();
	}
	wifi_status_pin = WIFI_GOOD;

	client_cfg.host = cfg.server;
	client_cfg.port = cfg.port;
	client_cfg.timeout_ms = cfg.http_tmo * 1000;

	client = esp_http_client_init(&client_cfg);
	if (client == NULL) {
		ESP_LOGE(TAG, "HTTP client init failed");
		status_and_sleep();
	}

	req_state.parser = XML_ParserCreate(NULL);
	XML_SetElementHandler(req_state.parser, xml_start_hndlr, dsl_end_hndlr);
	XML_SetCharacterDataHandler(req_state.parser, xml_data_hndlr);
	XML_SetUserData(req_state.parser, &xml_state);

	ESP_ERROR_CHECK(esp_http_client_set_header(
				client, "Accept", "text/xml"));
	ESP_ERROR_CHECK(esp_http_client_set_header(
				client, "Content-Type", "text/xml"));
	ESP_ERROR_CHECK(esp_http_client_set_header(
				client, "soapaction", DSL_SOAP_ACTION));
	ESP_ERROR_CHECK(esp_http_client_set_post_field(
				client, dsl_body, dsl_body_len));

	if ((ret = esp_http_client_perform(client)) != ESP_OK)
		ESP_LOGE(TAG, "DSL request failed: %d (%s)", ret,
			 esp_err_to_name(ret));

	xml_state.tag_name = IP_STATUS_TAG;
	xml_state.len = 0;
	xml_state.in_status = false;

	XML_ParserFree(req_state.parser);
	req_state.parser = XML_ParserCreate(NULL);
	XML_SetElementHandler(req_state.parser, xml_start_hndlr, ip_end_hndlr);
	XML_SetCharacterDataHandler(req_state.parser, xml_data_hndlr);
	XML_SetUserData(req_state.parser, &xml_state);

	req_state.name = "IP";
	ESP_ERROR_CHECK(esp_http_client_set_header(
				client, "soapaction", IP_SOAP_ACTION));
	ESP_ERROR_CHECK(esp_http_client_set_post_field(
				client, ip_body, ip_body_len));
	snprintf(url, URL_LEN, "http://%s:%u" IP_PATH, cfg.server, cfg.port);
	ESP_ERROR_CHECK(esp_http_client_set_url(client, url));

	if ((ret = esp_http_client_perform(client)) != ESP_OK)
		ESP_LOGE(TAG, "IP request failed: %d (%s)", ret,
			 esp_err_to_name(ret));

	XML_ParserFree(req_state.parser);
	if (esp_http_client_cleanup(client) != ESP_OK)
		ESP_LOGE(TAG, "HTTP client cleanup failed");

	status_and_sleep();
}
