/**
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 *
 */

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"

#include "lwip/err.h"
#include "lwip/sys.h"

static esp_event_handler_instance_t wifi_evt = NULL, ip_evt = NULL;
static TaskHandle_t taskToNotify = NULL;

static const char *TAG = "wifi station";

static inline const char *
decode_reason(wifi_err_reason_t reason)
{
	switch (reason) {
#define CASE_REASON(REASON) case REASON: return (#REASON)
		CASE_REASON(WIFI_REASON_UNSPECIFIED);
		CASE_REASON(WIFI_REASON_AUTH_EXPIRE);
		CASE_REASON(WIFI_REASON_AUTH_LEAVE);
		CASE_REASON(WIFI_REASON_ASSOC_EXPIRE);
		CASE_REASON(WIFI_REASON_ASSOC_TOOMANY);
		CASE_REASON(WIFI_REASON_NOT_AUTHED);
		CASE_REASON(WIFI_REASON_NOT_ASSOCED);
		CASE_REASON(WIFI_REASON_ASSOC_LEAVE);
		CASE_REASON(WIFI_REASON_ASSOC_NOT_AUTHED);
		CASE_REASON(WIFI_REASON_DISASSOC_PWRCAP_BAD);
		CASE_REASON(WIFI_REASON_DISASSOC_SUPCHAN_BAD);
		CASE_REASON(WIFI_REASON_BSS_TRANSITION_DISASSOC);
		CASE_REASON(WIFI_REASON_IE_INVALID);
		CASE_REASON(WIFI_REASON_MIC_FAILURE);
		CASE_REASON(WIFI_REASON_4WAY_HANDSHAKE_TIMEOUT);
		CASE_REASON(WIFI_REASON_GROUP_KEY_UPDATE_TIMEOUT);
		CASE_REASON(WIFI_REASON_IE_IN_4WAY_DIFFERS);
		CASE_REASON(WIFI_REASON_GROUP_CIPHER_INVALID);
		CASE_REASON(WIFI_REASON_PAIRWISE_CIPHER_INVALID);
		CASE_REASON(WIFI_REASON_AKMP_INVALID);
		CASE_REASON(WIFI_REASON_UNSUPP_RSN_IE_VERSION);
		CASE_REASON(WIFI_REASON_INVALID_RSN_IE_CAP);
		CASE_REASON(WIFI_REASON_802_1X_AUTH_FAILED);
		CASE_REASON(WIFI_REASON_CIPHER_SUITE_REJECTED);
		CASE_REASON(WIFI_REASON_TDLS_PEER_UNREACHABLE);
		CASE_REASON(WIFI_REASON_TDLS_UNSPECIFIED);
		CASE_REASON(WIFI_REASON_SSP_REQUESTED_DISASSOC);
		CASE_REASON(WIFI_REASON_NO_SSP_ROAMING_AGREEMENT);
		CASE_REASON(WIFI_REASON_BAD_CIPHER_OR_AKM);
		CASE_REASON(WIFI_REASON_NOT_AUTHORIZED_THIS_LOCATION);
		CASE_REASON(WIFI_REASON_SERVICE_CHANGE_PERCLUDES_TS);
		CASE_REASON(WIFI_REASON_UNSPECIFIED_QOS);
		CASE_REASON(WIFI_REASON_NOT_ENOUGH_BANDWIDTH);
		CASE_REASON(WIFI_REASON_MISSING_ACKS);
		CASE_REASON(WIFI_REASON_EXCEEDED_TXOP);
		CASE_REASON(WIFI_REASON_STA_LEAVING);
		CASE_REASON(WIFI_REASON_END_BA);
		CASE_REASON(WIFI_REASON_UNKNOWN_BA);
		CASE_REASON(WIFI_REASON_TIMEOUT);
		CASE_REASON(WIFI_REASON_PEER_INITIATED);
		CASE_REASON(WIFI_REASON_AP_INITIATED);
		CASE_REASON(WIFI_REASON_INVALID_FT_ACTION_FRAME_COUNT);
		CASE_REASON(WIFI_REASON_INVALID_PMKID);
		CASE_REASON(WIFI_REASON_INVALID_MDE);
		CASE_REASON(WIFI_REASON_INVALID_FTE);
		CASE_REASON(WIFI_REASON_TRANSMISSION_LINK_ESTABLISH_FAILED);
		CASE_REASON(WIFI_REASON_ALTERATIVE_CHANNEL_OCCUPIED);
		CASE_REASON(WIFI_REASON_BEACON_TIMEOUT);
		CASE_REASON(WIFI_REASON_NO_AP_FOUND);
		CASE_REASON(WIFI_REASON_AUTH_FAIL);
		CASE_REASON(WIFI_REASON_ASSOC_FAIL);
		CASE_REASON(WIFI_REASON_HANDSHAKE_TIMEOUT);
		CASE_REASON(WIFI_REASON_CONNECTION_FAIL);
		CASE_REASON(WIFI_REASON_AP_TSF_RESET);
		CASE_REASON(WIFI_REASON_ROAMING);
		CASE_REASON(WIFI_REASON_ASSOC_COMEBACK_TIME_TOO_LONG);
		CASE_REASON(WIFI_REASON_SA_QUERY_TIMEOUT);
#undef CASE_REASON
	}
	return ("INVALID REASON");
}

static void
event_handler(void *arg, esp_event_base_t event_base, int32_t event_id,
	      void *event_data)
{
	wifi_event_sta_connected_t *cx_evt;
	wifi_event_sta_disconnected_t *discx_evt;
	ip_event_got_ip_t *got_ip_evt;

	configASSERT(taskToNotify != NULL);
	if (event_base == WIFI_EVENT) {
		switch (event_id) {
		case WIFI_EVENT_STA_START:
			ESP_LOGI(TAG, "Station mode started, connecting ...");
			esp_wifi_connect();
			break;
		case WIFI_EVENT_STA_CONNECTED:
			cx_evt = (wifi_event_sta_connected_t *)event_data;
			ESP_LOGI(TAG, "Connected to AP %.*s channel %u",
				 cx_evt->ssid_len, cx_evt->ssid,
				 cx_evt->channel);
			break;
		case WIFI_EVENT_STA_DISCONNECTED:
			discx_evt =
				(wifi_event_sta_disconnected_t *)event_data;
			ESP_LOGW(TAG, "Disconnected from AP %.*s, "
				 "reason: %u (%s)", discx_evt->ssid_len,
				 discx_evt->ssid, discx_evt->reason,
				 decode_reason(discx_evt->reason));
			ESP_LOGW(TAG, "Attempting reconnect ...");
			esp_wifi_connect();
			break;
		}
	}
	else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
		got_ip_evt = (ip_event_got_ip_t*) event_data;
		ESP_LOGI(TAG, "got ipv4: " IPSTR,
			 IP2STR(&got_ip_evt->ip_info.ip));
		(void)xTaskNotifyGive(taskToNotify);
	}
}

esp_err_t
wifi_init_sta(const unsigned char ssid[], const unsigned char wifi_pass[],
	      int timeout_sec)
{
	size_t ssid_len;
	uint32_t val;
	TickType_t tmo_ticks = (timeout_sec * 1000) / portTICK_PERIOD_MS;

	taskToNotify = xTaskGetCurrentTaskHandle();

	(void)esp_netif_create_default_wifi_sta();

	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));

	ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
							    ESP_EVENT_ANY_ID,
							    &event_handler,
							    NULL, &wifi_evt));
	ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
							    IP_EVENT_STA_GOT_IP,
							    &event_handler,
							    NULL, &ip_evt));

	wifi_config_t wifi_config = {
		.sta = {
			/*
			 * Setting a password implies station will connect to
			 * all security modes including WEP/WPA.  However these
			 * modes are deprecated and not advisable to be
			 * used. Incase your Access point doesn't support WPA2,
			 * these mode can be enabled by commenting below line
			 */
			.threshold.authmode = WIFI_AUTH_WPA2_PSK,

			.listen_interval = CONFIG_WIFI_LISTEN_INTERVAL,
			.pmf_cfg = {
				.capable = true,
				.required = false
			},
		},
	};
	ssid_len = strnlen((const char *)ssid, MAX_SSID_LEN);
	memcpy(wifi_config.sta.ssid, ssid, ssid_len);
	strncpy((char *)wifi_config.sta.password, (const char *)wifi_pass,
		MAX_PASSPHRASE_LEN - 1);
	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
	ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
	ESP_LOGI(TAG, "Connecting to %s (timeout %ds) ...", ssid, timeout_sec);
	ESP_ERROR_CHECK(esp_wifi_start());
	(void)esp_wifi_set_ps(WIFI_PS_MAX_MODEM);

	ESP_LOGI(TAG, "Waiting for IP(s)");
	val = ulTaskNotifyTake(pdTRUE, tmo_ticks);

	if (val == 0) {
		ESP_LOGE(TAG, "Timed out attempt to connect to AP: %s", ssid);
		return (ESP_FAIL);
	}
	ESP_LOGI(TAG, "Connected to AP: %s", ssid);
	return (ESP_OK);
}

esp_err_t
wifi_stop_events(void)
{
	esp_err_t ret;

	if (wifi_evt != NULL)
		if ((ret = esp_event_handler_instance_unregister(
			     WIFI_EVENT, ESP_EVENT_ANY_ID, wifi_evt))
		    != ESP_OK)
			return (ret);
	if (ip_evt == NULL)
		return (ESP_OK);
	return (esp_event_handler_instance_unregister(
			IP_EVENT, IP_EVENT_STA_GOT_IP, ip_evt));
}

void
wifi_init_softap(void)
{
	wifi_config_t wifi_config = {
		.ap = {
			.ssid = CONFIG_WIFI_SSID,
			.ssid_len = strlen(CONFIG_WIFI_SSID),
			.channel = CONFIG_WIFI_CHANNEL,
			.password = CONFIG_WIFI_PASSWORD,
			.max_connection = CONFIG_WIFI_MAX_STA_CONN,
			.authmode = WIFI_AUTH_WPA_WPA2_PSK
		},
	};
	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();

	(void)esp_netif_create_default_wifi_ap();

	ESP_ERROR_CHECK(esp_wifi_init(&cfg));

	if (strlen(CONFIG_WIFI_PASSWORD) == 0)
		wifi_config.ap.authmode = WIFI_AUTH_OPEN;

	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
	ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &wifi_config));
	ESP_ERROR_CHECK(esp_wifi_start());

	ESP_LOGI("wifi softAP", "AP started: SSID:%s channel:%d",
		 CONFIG_WIFI_SSID, CONFIG_WIFI_CHANNEL);
}
