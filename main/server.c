/**
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#define LOG_LOCAL_LEVEL CONFIG_LOG_DEFAULT_LEVEL

#include <stdlib.h>
#include <errno.h>

#include <esp_wifi.h>
#include <esp_event.h>
#include <esp_log.h>
#include <esp_system.h>
#include <nvs_flash.h>
#include <sys/param.h>
#include "nvs_flash.h"
#include "esp_netif.h"
#include "esp_eth.h"
#include "esp_ota_ops.h"
#include "driver/gpio.h"

#include <esp_http_server.h>

#include "wifi.h"
#include "storage.h"

#if defined(CONFIG_COMPILER_OPTIMIZATION_SIZE) || \
	defined(CONFIG_COMPILER_OPTIMIZATION_PERF)
/* This is a non-debug release, set longer TTLs. */
#define CACHE_CTRL "public, max-age=604800, stale-while-revalidate=86400"
#else
#define CACHE_CTRL "public, max-age=300, stale-while-revalidate=60"
#endif

#define SERVER_STACK_SZ	      (8192)
#define POST_BODY_MAXLEN      (1024)
#define PARAM_MAXLEN          (HOST_NAME_MAX + 1)
#define MAX_PORT_LEN          (sizeof("65535") + 1)
#define MAX_64BIT_DECIMAL_LEN (sizeof("18446744073709551615") + 1)
#define MAX_32BIT_DECIMAL_LEN (sizeof("4294967296") + 1)
/* length of esp_app_desc_t version field + NUL */
#define ETAG_LEN              (33)

extern const char   index_body[]       asm("_binary_index_html_start");
extern const char   index_gz_body[]    asm("_binary_index_html_gz_start");
extern const char   settings_body[]    asm("_binary_settings_html_start");
extern const char   settings_gz_body[] asm("_binary_settings_html_gz_start");
extern const size_t index_len          asm("index_html_length");
extern const size_t index_gz_len       asm("index_html_gz_length");
extern const size_t settings_len       asm("settings_html_length");
extern const size_t settings_gz_len    asm("settings_html_gz_length");

static const char *TAG = "server";
static httpd_handle_t server = NULL;
static nvs_handle_t nvs_hndl;
static char req_body[POST_BODY_MAXLEN];
static char param_buf[PARAM_MAXLEN];
static char etag[ETAG_LEN];

static inline void
req_log(const char *method, httpd_req_t *req)
{
	ESP_LOGI("http", "%s %s", method, req->uri);
}

static esp_err_t
static_handler(httpd_req_t *req, const char body[], const char gz_body[],
	       const size_t len, const size_t gz_len)
{
	size_t sz;
	char hdr[CONFIG_HTTPD_MAX_REQ_HDR_LEN];
	const char *start;
	esp_err_t ret;

	httpd_resp_set_type(req, "text/html");

	start = body;
	sz = len;
	ret = httpd_req_get_hdr_value_str(req, "Accept-Encoding", hdr,
					  CONFIG_HTTPD_MAX_REQ_HDR_LEN);
	if (ret == ESP_OK && strstr(hdr, "gzip")) {
		ESP_ERROR_CHECK(httpd_resp_set_hdr(req, "Content-Encoding",
						   "gzip"));
		start = gz_body;
		sz = gz_len;
	}
	else if (ret != ESP_ERR_NOT_FOUND) {
		ESP_LOGE(TAG, "Error reading Accept-Encoding header: "
			 "%d (%s)", ret, esp_err_to_name(ret));
		ESP_ERROR_CHECK(httpd_resp_send_500(req));
		return (ESP_FAIL);
	}

	httpd_resp_send(req, start, sz);
	return (ESP_OK);
}

static esp_err_t
index_handler(httpd_req_t *req)
{
	char hdr[CONFIG_HTTPD_MAX_REQ_HDR_LEN];
	esp_err_t ret;

	req_log("GET", req);
	ret = httpd_req_get_hdr_value_str(req, "If-None-Match", hdr,
					  CONFIG_HTTPD_MAX_REQ_HDR_LEN);
	if (ret == ESP_OK && strcmp(hdr, etag) == 0) {
		httpd_resp_set_status(req, "304 Not Modified");
		httpd_resp_sendstr(req, "");
		return (ESP_OK);
	}
	if (ret != ESP_OK && ret != ESP_ERR_NOT_FOUND) {
		ESP_LOGE(TAG, "Error reading If-None-Match header: %d (%s)",
			 ret, esp_err_to_name(ret));
		ESP_ERROR_CHECK(httpd_resp_send_500(req));
		return (ESP_FAIL);
	}
	ESP_ERROR_CHECK(httpd_resp_set_hdr(req, "ETag", etag));
	ESP_ERROR_CHECK(httpd_resp_set_hdr(req, "Cache-Control", CACHE_CTRL));

	return (static_handler(req, index_body, index_gz_body, index_len,
			       index_gz_len));
}

static const httpd_uri_t home_get = {
	.uri       = "/",
	.method    = HTTP_GET,
	.handler   = index_handler,
	.user_ctx  = NULL
};

static const httpd_uri_t index_get = {
	.uri       = "/index.html",
	.method    = HTTP_GET,
	.handler   = index_handler,
	.user_ctx  = NULL
};

static esp_err_t
get_req_body(httpd_req_t *req, char *buf, size_t sz, size_t *len)
{
	int ret;

	*len = req->content_len;
	if (*len >= sz)
		return (ESP_ERR_NO_MEM);

	for (size_t l = *len; l > 0; l -= ret) {
		if ((ret = httpd_req_recv(req, buf, l)) <= 0) {
			if (ret == HTTPD_SOCK_ERR_TIMEOUT)
				continue;
			ESP_LOGE(TAG, "Error receiving request body: %d (%s)",
				 ret, esp_err_to_name(ret));
			return (ESP_FAIL);
		}
	}
	return (ESP_OK);
}

static esp_err_t
get_post_param(httpd_req_t *req, const char *name, size_t max)
{
	esp_err_t ret;

	if ((ret = httpd_query_key_value(req_body, name, param_buf, max))
	    != ESP_OK) {
		if (ret == ESP_ERR_NOT_FOUND) {
			ESP_LOGW(TAG, "%s not found in POST body", name);
			return (httpd_resp_send_err(req, 400, NULL));
		}
		else {
			ESP_LOGE(TAG, "Error reading %s from POST body: %d (%s)",
				 name, ret, esp_err_to_name(ret));
			ret = httpd_resp_send_500(req);
			return (ret);
		}
	}
	return (ESP_OK);
}

static esp_err_t
settings_post_handler(httpd_req_t *req)
{
	size_t len;
	char *endptr;
	esp_err_t ret;

	req_log("POST", req);
	if ((ret = get_req_body(req, req_body, POST_BODY_MAXLEN, &len))
	    != ESP_OK) {
		if (ret == ESP_ERR_NO_MEM)
			return (httpd_resp_send_err(req, 413, NULL));
		return (ret);
	}
	ESP_LOGD(TAG, "Request body: %.*s", len, req_body);
	req_body[len] = '\0';

	if ((ret = get_post_param(req, "ssid", MAX_SSID_LEN)) != ESP_OK)
		return (ret);
	if (param_buf[0] == '\0') {
		ESP_LOGW(TAG, "Empty ssid in POST body");
		return (httpd_resp_send_err(req, 400, NULL));
	}
	strcpy(cfg.ssid, param_buf);

	if ((ret = get_post_param(req, "pass", MAX_PASSPHRASE_LEN)) != ESP_OK)
		return (ret);
	if (strlen(param_buf) < 8) {
		ESP_LOGW(TAG, "wifi password in POST body too short "
			 "(length %zd)", strlen(param_buf));
		return (httpd_resp_send_err(req, 400, NULL));
	}
	strcpy(cfg.pass, param_buf);

	if ((ret = get_post_param(req, "tr064_srv", HOST_NAME_MAX)) != ESP_OK)
		return (ret);
	if (param_buf[0] == '\0') {
		ESP_LOGW(TAG, "Empty TR-064 host name in POST body");
		return (httpd_resp_send_err(req, 400, NULL));
	}
	strcpy(cfg.server, param_buf);

	if ((ret = get_post_param(req, "tr064_port", HOST_NAME_MAX)) != ESP_OK)
		return (ret);
	if (param_buf[0] == '\0') {
		ESP_LOGW(TAG, "Empty TR-064 port in POST body");
		return (httpd_resp_send_err(req, 400, NULL));
	}
	errno = 0;
	cfg.port = (uint16_t)strtoul(param_buf, &endptr, 10);
	if (errno != 0) {
		ESP_LOGW(TAG, "Cannot convert TR-064 port: %s", strerror(errno));
		return (httpd_resp_send_err(req, 400, NULL));
	}
	if (*endptr != '\0') {
		ESP_LOGW(TAG, "Cannot convert TR-064 port: "
			 "invalid trailing characters '%s'", endptr);
		return (httpd_resp_send_err(req, 400, NULL));
	}

	if ((ret = get_post_param(req, "sleep_intvl", MAX_64BIT_DECIMAL_LEN))
	    != ESP_OK)
		return (ret);
	if (param_buf[0] == '\0') {
		ESP_LOGW(TAG, "Empty sleep duration in POST body");
		return (httpd_resp_send_err(req, 400, NULL));
	}
	errno = 0;
	cfg.sleep_intvl = (uint64_t)strtoul(param_buf, &endptr, 10);
	if (errno != 0) {
		ESP_LOGW(TAG, "Cannot convert sleep duration: %s",
			 strerror(errno));
		return (httpd_resp_send_err(req, 400, NULL));
	}
	if (*endptr != '\0') {
		ESP_LOGW(TAG, "Cannot convert sleep duration: "
			 "invalid trailing characters '%s'", endptr);
		return (httpd_resp_send_err(req, 400, NULL));
	}

	if ((ret = get_post_param(req, "wifi_tmo", MAX_64BIT_DECIMAL_LEN))
	    != ESP_OK)
		return (ret);
	if (param_buf[0] == '\0') {
		ESP_LOGW(TAG, "Empty WiFi timeout in POST body");
		return (httpd_resp_send_err(req, 400, NULL));
	}
	errno = 0;
	cfg.wifi_tmo = (int64_t)strtol(param_buf, &endptr, 10);
	if (errno != 0) {
		ESP_LOGW(TAG, "Cannot convert WiFi timeout: %s",
			 strerror(errno));
		return (httpd_resp_send_err(req, 400, NULL));
	}
	if (*endptr != '\0') {
		ESP_LOGW(TAG, "Cannot convert WiFi timeout: "
			 "invalid trailing characters '%s'", endptr);
		return (httpd_resp_send_err(req, 400, NULL));
	}

	if ((ret = get_post_param(req, "tr064_tmo", MAX_32BIT_DECIMAL_LEN))
	    != ESP_OK)
		return (ret);
	if (param_buf[0] == '\0') {
		ESP_LOGW(TAG, "Empty TR-064 HTTP timeout in POST body");
		return (httpd_resp_send_err(req, 400, NULL));
	}
	errno = 0;
	cfg.http_tmo = (int32_t)strtol(param_buf, &endptr, 10);
	if (errno != 0) {
		ESP_LOGW(TAG, "Cannot convert TR-064 HTTP timeout: %s",
			 strerror(errno));
		return (httpd_resp_send_err(req, 400, NULL));
	}
	if (*endptr != '\0') {
		ESP_LOGW(TAG, "Cannot convert TR-064 HTTP timeout: "
			 "invalid trailing characters '%s'", endptr);
		return (httpd_resp_send_err(req, 400, NULL));
	}

	if (set_config(nvs_hndl, &cfg) != ESP_OK) {
		ESP_LOGE(TAG, "Error saving config in NVS");
		return (httpd_resp_send_500(req));
	}

	ret = static_handler(req, settings_body, settings_gz_body, settings_len,
			     settings_gz_len);
	if (ret != ESP_OK)
		return (ret);

	ESP_LOGI(TAG, "Closing NVS");
	nvs_close(nvs_hndl);
	ESP_LOGI(TAG, "Stopping WiFi");
	ESP_ERROR_CHECK(esp_wifi_stop());
	ESP_ERROR_CHECK(esp_wifi_deinit());
	ESP_ERROR_CHECK(gpio_set_level(CONFIG_GPIO_RUN, 0));
	ESP_LOGI(TAG, "Restarting");
	esp_restart();

	/* Unreachable */
	return (ESP_OK);
}

static const httpd_uri_t settings_post = {
	.uri       = "/settings",
	.method    = HTTP_POST,
	.handler   = settings_post_handler,
	.user_ctx  = NULL
};

void
ap_mode(void)
{
	const esp_app_desc_t *app_desc;
	httpd_config_t config = HTTPD_DEFAULT_CONFIG();
	esp_err_t err;

	ESP_ERROR_CHECK(nvs_open("settingsNS", NVS_READWRITE, &nvs_hndl));

	wifi_init_softap();

	config.lru_purge_enable = true;
	config.stack_size = SERVER_STACK_SZ;
	ESP_LOGI(TAG, "Starting server on port: %d", config.server_port);
	if ((err = httpd_start(&server, &config)) != ESP_OK) {
		ESP_LOGE(TAG, "Error starting server: %s",
			 esp_err_to_name(err));
		esp_system_abort("HTTP server failure");
	}

	app_desc = esp_app_get_description();
	assert(app_desc != NULL);
	snprintf(etag, ETAG_LEN, "%s", app_desc->version);

	httpd_register_uri_handler(server, &home_get);
	httpd_register_uri_handler(server, &index_get);
	httpd_register_uri_handler(server, &settings_post);

	ESP_LOGI(TAG, "Server ready");
}
