/**
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <string.h>

#define LOG_LOCAL_LEVEL ESP_LOG_DEBUG
#include <esp_log.h>

#include "storage.h"

static const char *TAG = "nvs";

#define GET(hndl, ret, s, fld, type)				\
	do {							\
		ret = nvs_get_##type(hndl, #fld, &s->fld);	\
		if (ret != ESP_OK) {				\
			ESP_LOGW(TAG, #fld ": %d (%s)", ret,	\
				 esp_err_to_name(ret));		\
			return (ret);				\
		}						\
	} while (0)

#define GET_STR(hndl, ret, s, fld, len)				\
	do {							\
		ret = nvs_get_str(hndl, #fld, s->fld, &len);	\
		if (ret != ESP_OK) {				\
			ESP_LOGW(TAG, #fld ": %d (%s)", ret,	\
				 esp_err_to_name(ret));		\
			return (ret);				\
		}						\
	} while (0)

#define SET(hndl, ret, s, fld, type)					\
	do {								\
		ret = nvs_set_##type(hndl, #fld, s->fld);	\
		if (ret != ESP_OK)					\
			return (ret);					\
		ret = nvs_commit(hndl);					\
		if (ret != ESP_OK)					\
			return (ret);					\
	} while (0)

esp_err_t
get_config(nvs_handle_t hndl, struct config *cfg)
{
	esp_err_t ret;
	size_t len;

	len = MAX_SSID_LEN;
	GET_STR(hndl, ret, cfg, ssid, len);
	len = MAX_PASSPHRASE_LEN;
	GET_STR(hndl, ret, cfg, pass, len);
	len = HOST_NAME_MAX;
	GET_STR(hndl, ret, cfg, server, len);
	GET(hndl, ret, cfg, sleep_intvl, u64);
	GET(hndl, ret, cfg, wifi_tmo, i64);
	GET(hndl, ret, cfg, http_tmo, i32);
	GET(hndl, ret, cfg, port, u16);

	return (ESP_OK);
}

esp_err_t
set_config(nvs_handle_t hndl, struct config *cfg)
{
	esp_err_t ret;

	SET(hndl, ret, cfg, ssid, str);
	SET(hndl, ret, cfg, pass, str);
	SET(hndl, ret, cfg, server, str);
	SET(hndl, ret, cfg, sleep_intvl, u64);
	SET(hndl, ret, cfg, wifi_tmo, i64);
	SET(hndl, ret, cfg, http_tmo, i32);
	SET(hndl, ret, cfg, port, u16);

	return (ESP_OK);
}
