/**
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdint.h>

#include <nvs.h>
#include <esp_wifi_types.h>

/* Can't seem to get this from limits.h */
#define HOST_NAME_MAX (253)

struct config {
	char		server[HOST_NAME_MAX];
	char		pass[MAX_PASSPHRASE_LEN];
	char		ssid[MAX_SSID_LEN];
	uint64_t	sleep_intvl;
	int64_t		wifi_tmo;
	int32_t		http_tmo;
	uint16_t	port;
};

extern struct config cfg;

esp_err_t get_config(nvs_handle_t hndl, struct config *cfg);
esp_err_t set_config(nvs_handle_t hndl, struct config *cfg);
