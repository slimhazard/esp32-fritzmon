# esp32-fritzmon #

esp32-fritzmon is a device based on Espressif's
[ESP32](https://www.espressif.com/en/products/socs/esp32) microcontroller that
provides status indicators for a home WiFi network. The device works in networks
with a router that provides the
[TR-064](https://www.broadband-forum.org/technical/download/TR-064_Issue-2.pdf)
interface, such as the [AVM FRITZ!Box](https://avm.de/service/schnittstellen/).

esp32-fritzmon runs periodically and lights three LED indicators, green or red
for the "good" or "down" states:

  * WiFi: good (LED green) if a login to the network was successful
  
  * DSL: good if the link is up (a connection to the provider over telephone
    lines is established)
  
  * IP: good if the WAN is connected (an IP address to the Internet-facing
    network is assigned, so that connections to the Internet are possible)

If any of these conditions do not hold, the corresponding indicator signals
"down" (LED red).

After the checks run, the ESP32 goes into deep sleep, by default for 15
seconds. When the device wakes up the cycle begins again.

The device also has a "run" LED that flashes briefly before the checks begin,
just after initial startup and after wakeup from deep sleep.

esp32-fritzmon is designed for mobility -- a small device with low energy
consumption, suitable for battery power.

![esp32-fritzmon-perfboard](https://gitlab.com/slimhazard/esp32-fritzmon/-/wikis/uploads/29cee969a0928ac0cf1d20c5a886526f/fritzmon-perfboard-annotated.jpg "esp32-fritzmon on a perfboard")

See video of esp32-fritzmon running on a [perfboard](https://gitlab.com/slimhazard/esp32-fritzmon/-/wikis/esp32-fritzmon-on-a-perfboard "esp32-fritzmon running on a perfboard")
and on a [breadboard](https://gitlab.com/slimhazard/esp32-fritzmon/-/wikis/esp32-fritzmon-on-a-breadboard "esp32-fritzmon running on a breadboard")

## Configuration ##

esp32-fritzmon must be configured with the SSID (network name) and password of
the WiFi network that it monitors, along with a few other optional configuration
parameters. It stores its configuration persistently, so that the data are
available after restart and after wakeup from deep sleep. But the initial
configuration must be set at least once when the device starts up the first
time.

When esp32-fritzmon boots up the first time (or when re-configuration is
requested), it starts in AP mode -- that is, it provides its own WiFi
network (AP for access point). By default, the credentials for this network are:

  * SSID (network name): `esp32-fritzmon`
  * Password: `fritzmon`

(These can be changed in the [configuration](#code-configuration) of the
firmware build.)

You can log into that network with any device, and then visit the address
`http://192.168.4.1` in a web browser, which brings up the configuration page.

The tl;dr version of configuration settings is that you must set the SSID (or
name) and password of the WiFi network to be monitored, and that other settings
may likely be left at the defaults.

The longer version is that the settings have these fields:

  * WiFi network to be monitored
    * SSID
    * Password
  * TR-064 endpoint hostname or IP address and port number, defaults
    `fritz.box` and 49000.  This is the endpoint to which the DSL and
    IP status requests are sent. The defaults are valid for all
    FRITZ!Box routers.
  * Timeouts
    * WiFi connection: how long to attempt the network login before giving up.  
      Default 15 seconds
    * TR-064 HTTP timeout: timeout for the DSL and IP status requests.  
      Default 10 seconds
    * Deep sleep time: time until wakeup to repeat the checks.  
      Default 15 seconds

After entering the configuration, press the "Save and run" button. The device
will restart and begin monitoring the network. The settings are stored on the
device and will be re-used from then on.

esp32-fritzmon has a "configuration request" button, a momentary tactile switch.
If the button is held down at boot time (power up or wakeup after sleep), the
device will enter AP mode as described above, so that a new configuration can be
set.

The device can also be re-configured by [loading the firmware](#quick-start),
including an empty data partition for the configuration.

## Hardware ##

For the status indicators (three groups of two LEDs), it makes sense
to use bicolor red/green LEDs with a common cathode, to minimize the
component count. Only one of the red/green LEDs is ever turned on at a
time, so the rating of the current-limiting resistor at the cathode
needs to be no higher than the highest rating needed for the pair.

By default, components are connected to the ESP32's pins as follows
(the assignments can be [changed in the firmware build](#gpio-configuration)):

<a name="GPIO_config"></a>

|                         | ESP32C3 |                               |
|:------------------------|:--------|:------------------------------|
| WiFi "good" (green LED) | GPIO4   |                               |
| WiFi "down" (red LED)   | GPIO5   |                               |
| DSL "good"              | GPIO6   |                               |
| DSL "down"              | GPIO7   |                               |
| IP "good"               | GPIO18  |                               |
| IP "down"               | GPIO3   |                               |
| run LED                 | GPIO10  |                               |
| config request -        | GPIO1   | and GND via pulldown resistor |
| config request +        | 3V3     |                               |
| all LED cathodes        | GND     | via pulldown resistors        |

The "config request" button is a normally-open momentary tactile
switch. It is active high -- re-configuration is requested if it is
held down when the device starts or wakes up, initiating AP mode as
described above. So one of its paths should be connected to the GPIO
pin on one end and to GND via a pulldown resistor on the other, and
the other path to 3V3.

This schematic shows the circuit for a build with Espressif's
[ESP32-C3-DevKitM-1](https://docs.espressif.com/projects/esp-idf/en/latest/esp32c3/hw-reference/esp32c3/user-guide-devkitm-1.html)
development board on a breadboard or perfboard. It will work with any of the
many dev board clones:

![Devboard/breadboard/perfboard](docs/hardware/esp32-fritzmon/esp32-fritzmon-devkit.svg "Schematic for a dev board on breadboard or perfboard")

With a dev board, the circuit fits on a 10 x 24 hole perfboard (3cm x 7cm), with
the resistors mounted vertically. The drawing below (derived from a
[Fritzing diagram](docs/hardware/fritzing/perfboard.fzz "Fritzing diagram"))
shows the wiring on such a perfboard to connect with female pin
headers (15 pins each) that fit an ESP32-C3-DevKitM-1. The board is
attached so that the micro USB connector points up on the diagram, and
the ESP32 module with antenna points down.

![fritzing_perfboard](https://gitlab.com/slimhazard/esp32-fritzmon/-/wikis/uploads/b39f7d6fec499674e66c9469db186e88/perfboard.svg "Fritzing perfboard diagram")

## Firmware ##

The code for esp32-fritzmon is written in C for
[esp-idf](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html),
Espressif's development framework.


### Requirements ###

  * esp-idf
  
    The build has been tested with version
    [5.1.2](https://github.com/espressif/esp-idf/releases/tag/v5.1.2).
    The installation and use of esp-idf is beyond the scope of this
    documentation, see the
    [manual](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html)
    for details.
    
  * [expat](https://libexpat.github.io/) XML parser, used to parse responses
    to the DSL and IP status requests.
    
    Since esp-idf version 5, expat has been moved from esp-idf itself to the
    [IDF Component Registry](https://components.espressif.com/). expat must be
    added to this project with the
    [`idf.py add-dependency`](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-guides/tools/idf-component-manager.html#using-with-a-project)
    command:

```shell
$ idf.py add-dependency "espressif/expat^2.5.0~1"
```

  * [minify](https://github.com/tdewolff/minify), used to reduce the size of
    web pages used in AP mode to set configuration. (This is the Go program
    called minify, not the Node.js package.)
    
    minify is available on most Linux distributions (as the package `minify`).
    On MacOS it can be installed with `brew install tdewolff/tap/minify`. If
    you don't have a way to install minify as a package, it can be
    [built from source or installed with `go install`](https://github.com/tdewolff/minify/tree/master/cmd/minify#installation),
    which in turn requires installations of [go](https://go.dev/doc/install)
    and [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

  * gzip

### Quick start ###

Software builds are driven by esp-idf's
[idf.py](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-guides/build-system.html#idf-py)
tool:

``` shell
# Optionally specify the build target -- the ESP32 chip.
# Possible targets are esp32, esp32c3, esp32s2 and esp32s3, default esp32.
$ idf.py set-target <target>

# Optionally use the menuconfig command to configure the build.
# This step may be skipped to build with defaults.
# For details see "Code configuration" below.
$ idf.py menuconfig

# Build the firmware.
$ idf.py build

# Flash the firmware to the ESP32.
# This command also overwrites the NVS partiton (non-volatile storage)
# that persistently stores configuration data, initially with default
# values. The WiFi SSID and password are unset by default, so the
# application will start in AP mode, as described above.
$ idf.py flash -p <port>

# Flash the firmware only, without overwriting the config partition.
$ idf.py app-flash -p <port>

# Restart the application and view its log output
$ idf.py monitor -p <port>
```

Firmware must be flashed with the `idf.py flash` command the first time,
so that the initial, empty configuration is loaded. The `idf.py app-flash`
command is convenient during development, if you have already set a
configuration and need to test an new version of the code. With `app-flash`,
the config partition is not overwritten and the application can start in station
mode, so you don't need to enter the configuration again.

### Code configuration ###

This project uses esp-idf's configuration tool invoked by the
`idf.py menuconfig` command for
[compile-time project configuration](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/kconfig.html).
esp32-fritzmon adds two items to the main menu:

  * WiFi configuration
  * GPIO configuration

#### WiFi configuration ####

This configuration sets properties of WiFi in both AP mode (for runtime
configuration) and station mode (for regular operation).

  * *WiFi SSID*

    The SSID (network name) used in AP mode. The maximum length of an SSID
    is 32 characters.
    
    Default: `esp32-fritzmon`
    
  * *WiFi password*
  
    WPA or WPA2 password used in AP mode. The password must be at least 8
    and at most 63 characters long.
    
    Default: `fritzmon`
    
  * *WiFi channel*
  
    The WiFi channel used in AP mode. Must be an integer from 1 to 13,
    inclusive.
    
    Default: 1
    
  * *Maximum station connections*
  
    The maximum number of stations that connect when AP mode is running.
    
    Default: 1
    
  * *WiFi listen interval*
  
    This configuration is for station mode. It sets the number of beacon
    intervals at which the station listens for the AP's beacon frame. The
    beacon interval duration is set by the AP. For example, if the beacon
    interval is 100 ms and this value is set to 3, then the station listens
    for the beacon every 300 ms.
    
    The value represents a tradeoff between power savings and network
    stability. Listening for the beacon less often consumes less
    power, but not listening often enough risks connectivity issues
    for the station. For best results, the listen interval should be
    set so that the station listens every 250 ms to 400 ms.
    
    Default: 3

#### GPIO configuration ####

This configuration sets the GPIO pin numbers for various functions. The
defaults are as shown [above](#GPIO_config):

  * *WiFi good GPIO*
  * *WiFi down GPIO*
  * *DSL good GPIO*
  * *DSL down GPIO*
  * *IP good GPIO*
  * *IP down GPIO*
  * *Run GPIO*
  * *Re-configuration request input GPIO*
